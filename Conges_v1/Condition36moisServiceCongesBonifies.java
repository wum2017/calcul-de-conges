package Conges_v1;

import conditions.*;

public class Condition36moisServiceCongesBonifies implements ConditionI<Agent>
{
    private static boolean T = true;
    
    public boolean estSatisfaite(Agent a){
        // if(T) System.out.println("36 mois de service ininterrompu : " + (a.getMoisService()));
        return a.getMoisService() >= 36;
    }
}