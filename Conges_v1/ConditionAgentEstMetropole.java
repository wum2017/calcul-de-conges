package Conges_v1;

import conditions.*;

public class ConditionAgentEstMetropole implements ConditionI<Agent>
{
    private static boolean T = true;
    
    public boolean estSatisfaite(Agent a){
        // if(T) System.out.println("agent estMetropole : " + a.getEstMetropole());
        return a.getEstMetropole();
    }
}