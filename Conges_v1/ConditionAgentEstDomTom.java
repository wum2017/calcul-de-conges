package Conges_v1;

import conditions.*;

public class ConditionAgentEstDomTom implements ConditionI<Agent>
{
    private static boolean T = true;
    
    public boolean estSatisfaite(Agent a){
        // if(T) System.out.println("agent estDomTom : " + a.getEstDomTom());
        return a.getEstDomTom();
    }
}