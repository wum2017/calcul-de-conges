package Conges_v1;

import conditions.*;

public class ConditionConjointEtranger implements ConditionI<Agent>
{
    private static boolean T = true;
    
    public boolean estSatisfaite(Agent a){
        // if(T) System.out.println("conjoint estEtranger : " + !a.getPaysOrigineConjoint().equals("France"));
        return !a.getPaysOrigineConjoint().equals("France");
    }
}