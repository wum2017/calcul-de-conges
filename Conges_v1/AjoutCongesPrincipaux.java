package Conges_v1;

import commandes.*;

public class AjoutCongesPrincipaux implements CommandeI<Agent,Resultat>{
    private static boolean T = true;
    private float jours;
    
    public void setJours(float jours){
        this.jours = jours;
    }
    
    public boolean executer(Agent a, Resultat res){
        // if(T) System.out.println("Ajout jours congés principaux : " + this.jours);
        res.addJoursDeConges(this.jours);
        return true;
    }
}