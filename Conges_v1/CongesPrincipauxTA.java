package Conges_v1;

import commandes.*;

public class CongesPrincipauxTA implements CommandeI<Agent,Resultat>{
    public boolean executer(Agent a, Resultat res){
        float n = 0;
        float d = 0;
        for (Periode p : a.getPeriodesTravailles()){
            n += (float)p.getNbCinquieme()*(float)(p.getSemaineFin() - p.getSemaineDebut());
            d += (float)(p.getSemaineFin() - p.getSemaineDebut());
        }
        // if (d!=0) 
        n = 5*n/d; // d!=0 car l'agent doit travailler pendant au moins une période de l'année
        res.addJoursDeConges(n);
        return true;
    }
}