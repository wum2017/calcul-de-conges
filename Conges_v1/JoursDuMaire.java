package Conges_v1;

import commandes.*;

public class JoursDuMaire implements CommandeI<Agent,Resultat>{
    private static boolean T = true;
    private ConditionJoursDuMaire[] conditions;
   
    public void setConditions(ConditionJoursDuMaire[] conditions){
        this.conditions = conditions;
    }
   
    public boolean executer(Agent a, Resultat resultat){
        for(ConditionJoursDuMaire condition : conditions){
            boolean res = condition.estSatisfaite(a);
            if(res) resultat.addJoursDeConges(condition.getNombreJoursSupplementaires());
            // if(T)System.out.println("ConditionJoursDuMaire.estSatisfaite : " + res + " " + a.getAnciennete());
            if (res) return false;
        }
        return true;
    }
}