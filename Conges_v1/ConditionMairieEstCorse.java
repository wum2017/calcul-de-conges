package Conges_v1;

import conditions.*;
public class ConditionMairieEstCorse implements ConditionI<Agent>
{
    private static boolean T = true;
    
    public boolean estSatisfaite(Agent a){
        Mairie m = a.getMairie();
        // if(T) System.out.println("mairie estCorse : " + m.getEstCorse());
        return m.getEstCorse();
    }
}