package Conges_v1;

import conditions.*;

public class ConditionMairieEstMetropole implements ConditionI<Agent>
{
    private static boolean T = true;
    
    public boolean estSatisfaite(Agent a){
        Mairie m = a.getMairie();
        // if(T) System.out.println("mairie estMetropole : " + m.getEstMetropole());
        return m.getEstMetropole();
    }
}