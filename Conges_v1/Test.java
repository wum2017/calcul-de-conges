package Conges_v1;

import container.Factory;
import container.ApplicationContext;
import commandes.Invocateur;
import commandes.CommandeI;
import java.util.*;
import java.time.LocalDate;

public class Test extends junit.framework.TestCase
{
    public void testCongesAgent1() throws Exception{
        ApplicationContext container = Factory.createApplicationContext("./Conges_v1/README.TXT");
        
        Mairie m = new Mairie();
        m.setNom("Lyon");
        m.setEstMetropole(true);
        m.setEstDomTom(false); 
        m.setEstCorse(false);
        
        Agent a = new Agent();
        a.setNom("Dupont");
        a.setPrenom("Jacques");
        a.setDateDeNaissance(LocalDate.of(1998, 1, 23));
        a.setMairie(m);
        a.setTypeContrat("TC");
        a.setDebutContrat(LocalDate.of(2011, 2, 21));
        List<String> jours = new ArrayList();
        jours.add("Lundi");
        jours.add("Mardi");
        jours.add("Mercredi");
        jours.add("Jeudi");
        jours.add("Vendredi");
        a.setJoursTravailles(jours);
        a.setPaysOrigine("France");
        a.setDptOrigine("Guyane");
        a.setVilleOrigine("Cayenne");
        a.setPaysOrigineConjoint("France");
        a.setEstMetropole(false);
        a.setEstDomTom(true);
        a.setEstCorse(false);
        a.setJoursCongesUtilises(0);
        a.setPeriodesCongesPris(null);
        a.setJoursCongesCumules(25);
        a.setDemandeCongesBonifies(LocalDate.of(2014, 12, 1));
        
        Resultat resultat = new Resultat();
             
        try{
            Invocateur invocateur = container.getBean("invocateur");
            invocateur.executer(a, resultat);
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("Exception ! " + e.getMessage());
        }finally{
            System.out.println("resultat: Le nombre de jours de congés de l'agent est de : "+ resultat.getJoursDeConges() + "\n" +
                                "Le nombre de jours de congés sans solde de l'agent est de : " + resultat.getJoursDeCongesSansSolde() + "\n" +
                                "Le nombre de jours de congés groupés de l'agent est de : " + resultat.getJoursDeCongesGroupes() + "\n" +
                                "Le nombre de jours de congés utilisés de l'agent est de : " + resultat.getJoursDeCongesUtilises());
        }
    }
    
    
    public void testCongesAgent2() throws Exception{
        ApplicationContext container = Factory.createApplicationContext("./Conges_v1/README.TXT");
        Mairie m = new Mairie();
        m.setNom("Nice");
        m.setEstMetropole(true);
        m.setEstDomTom(false);
        m.setEstCorse(false);
        
        Agent a = new Agent();
        a.setNom("Lucas");
        a.setPrenom("Martin");
        a.setDateDeNaissance(LocalDate.of(1985, 2, 28));
        a.setMairie(m);
        a.setTypeContrat("TP");
        a.setDebutContrat(LocalDate.of(2005, 5, 1));
        List<String> jours = new ArrayList();
        jours.add("Lundi");
        jours.add("Jeudi");
        jours.add("Vendredi");
        a.setJoursTravailles(jours);
        a.setPaysOrigine("France");
        a.setDptOrigine("Alpes-Maritimes");
        a.setVilleOrigine("Nice");
        a.setPaysOrigineConjoint("Inde");
        a.setEstMetropole(true);
        a.setEstDomTom(false);
        a.setEstCorse(false);
        a.setJoursCongesUtilises(7);
        List<PeriodeConges> periodesCongesPris = new ArrayList();
        PeriodeConges p = new PeriodeConges(LocalDate.of(2018, 3, 12), LocalDate.of(2018, 3, 18));
        periodesCongesPris.add(p);
        a.setPeriodesCongesPris(periodesCongesPris);
        a.setJoursCongesCumules(0);
        a.setDemandeCongesBonifies(null);
        
        Resultat resultat = new Resultat();
        
        try{
            Invocateur invocateur = container.getBean("invocateur");
            invocateur.executer(a, resultat);
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("Exception ! " + e.getMessage());
        }finally{
            System.out.println("resultat: Le nombre de jours de congés de l'agent est de : "+ resultat.getJoursDeConges() + "\n" +
                                "Le nombre de jours de congés sans solde de l'agent est de : " + resultat.getJoursDeCongesSansSolde() + "\n" +
                                "Le nombre de jours de congés groupés de l'agent est de : " + resultat.getJoursDeCongesGroupes() + "\n" +
                                "Le nombre de jours de congés utilisés de l'agent est de : " + resultat.getJoursDeCongesUtilises());
        }
    }
    
    
    public void testCongesAgent3() throws Exception{
        ApplicationContext container = Factory.createApplicationContext("./Conges_v1/README.TXT");
        Mairie m = new Mairie();
        m.setNom("Koungou");
        m.setEstMetropole(false);
        m.setEstDomTom(true);
        m.setEstCorse(false);
        
        Agent a = new Agent();
        a.setNom("Fisher");
        a.setPrenom("Thomas");
        a.setDateDeNaissance(LocalDate.of(1983, 5, 22));
        a.setMairie(m);
        a.setTypeContrat("TNC");
        a.setDebutContrat(LocalDate.of(2008, 7, 1));
        List<String> jours = new ArrayList();
        jours.add("Lundi");
        jours.add("Mardi");
        jours.add("Jeudi");
        jours.add("Vendredi");
        a.setJoursTravailles(jours);
        a.setPaysOrigine("Angleterre");
        a.setDptOrigine("Angleterre du Nord-Ouest");
        a.setVilleOrigine("Liverpool");
        a.setPaysOrigineConjoint("Angleterre");
        a.setEstMetropole(false);
        a.setEstDomTom(false);
        a.setEstCorse(false);
        a.setJoursCongesUtilises(10);
        List<PeriodeConges> periodesCongesPris = new ArrayList();
        PeriodeConges p = new PeriodeConges(LocalDate.of(2018, 1, 8), LocalDate.of(2018, 1, 17));
        periodesCongesPris.add(p);
        a.setPeriodesCongesPris(periodesCongesPris);
        a.setJoursCongesCumules(30);
        a.setDemandeCongesBonifies(null);
        
        Resultat resultat = new Resultat();
        
        try{
            Invocateur invocateur = container.getBean("invocateur");
            invocateur.executer(a, resultat);
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("Exception ! " + e.getMessage());
        }finally{
            System.out.println("resultat: Le nombre de jours de congés de l'agent est de : "+ resultat.getJoursDeConges() + "\n" +
                                "Le nombre de jours de congés sans solde de l'agent est de : " + resultat.getJoursDeCongesSansSolde() + "\n" +
                                "Le nombre de jours de congés groupés de l'agent est de : " + resultat.getJoursDeCongesGroupes() + "\n" +
                                "Le nombre de jours de congés utilisés de l'agent est de : " + resultat.getJoursDeCongesUtilises());
        }
    }
    
    
    public void testCongesAgent4() throws Exception{
        ApplicationContext container = Factory.createApplicationContext("./Conges_v1/README.TXT");
        Mairie m = new Mairie();
        m.setNom("Isère");
        m.setEstMetropole(true);
        m.setEstDomTom(false);
        m.setEstCorse(false);
        
        Agent a = new Agent();
        a.setNom("Durand");
        a.setPrenom("Bernard");
        a.setDateDeNaissance(LocalDate.of(1964, 11, 8));
        a.setMairie(m);
        a.setTypeContrat("TA");
        a.setDebutContrat(LocalDate.of(2003, 3, 1));
        List<Periode> periodes = new ArrayList();
        Periode p = new Periode(8, 32, 3);
        periodes.add(p);
        p = new Periode(42, 46, 2);
        periodes.add(p);
        a.setPeriodesTravailles(periodes);
        a.setPaysOrigine("France");
        a.setDptOrigine("Corse");
        a.setVilleOrigine("Ajaccio");
        a.setPaysOrigineConjoint("France");
        a.setEstMetropole(true);
        a.setEstDomTom(false);
        a.setEstCorse(true);
        a.setJoursCongesUtilises(7);
        List<PeriodeConges> periodesCongesPris = new ArrayList();
        PeriodeConges pc = new PeriodeConges(LocalDate.of(2018, 5, 7), LocalDate.of(2018, 5, 13));
        periodesCongesPris.add(pc);
        a.setPeriodesCongesPris(periodesCongesPris);
        a.setJoursCongesCumules(0);
        a.setDemandeCongesBonifies(null);
        
        Resultat resultat = new Resultat();
        
        try{
            Invocateur invocateur = container.getBean("invocateur");
            invocateur.executer(a, resultat);
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("Exception ! " + e.getMessage());
        }finally{
            System.out.println("resultat: Le nombre de jours de congés de l'agent est de : "+ resultat.getJoursDeConges() + "\n" +
                                "Le nombre de jours de congés sans solde de l'agent est de : " + resultat.getJoursDeCongesSansSolde() + "\n" +
                                "Le nombre de jours de congés groupés de l'agent est de : " + resultat.getJoursDeCongesGroupes() + "\n" +
                                "Le nombre de jours de congés utilisés de l'agent est de : " + resultat.getJoursDeCongesUtilises());
        }
    }
    
    public void testCongesAgent5() throws Exception{
        ApplicationContext container = Factory.createApplicationContext("./Conges_v1/README.TXT");
        
        Mairie m = new Mairie();
        m.setNom("Nouméa"); // Nouvelle-Calédonie
        m.setEstMetropole(false);
        m.setEstDomTom(true); 
        m.setEstCorse(false);
        
        Agent a = new Agent();
        a.setNom("Boyer");
        a.setPrenom("Simon");
        a.setDateDeNaissance(LocalDate.of(1999, 6, 5));
        a.setMairie(m);
        a.setTypeContrat("TC");
        a.setDebutContrat(LocalDate.of(2018, 2, 1));
        List<String> jours = new ArrayList();
        jours.add("Lundi");
        jours.add("Mardi");
        jours.add("Mercredi");
        jours.add("Jeudi");
        jours.add("Vendredi");
        a.setJoursTravailles(jours);
        a.setPaysOrigine("France");
        a.setDptOrigine("Haute-Garonne");
        a.setVilleOrigine("Toulouse");
        a.setPaysOrigineConjoint("");
        a.setEstMetropole(true);
        a.setEstDomTom(false);
        a.setEstCorse(false);
        a.setJoursCongesUtilises(0);
        a.setPeriodesCongesPris(null);
        a.setJoursCongesCumules(0);
        a.setDemandeCongesBonifies(null);
        
        Resultat resultat = new Resultat();
             
        try{
            Invocateur invocateur = container.getBean("invocateur");
            invocateur.executer(a, resultat);
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("Exception ! " + e.getMessage());
        }finally{
            System.out.println("resultat: Le nombre de jours de congés de l'agent est de : "+ resultat.getJoursDeConges() + "\n" +
                                "Le nombre de jours de congés sans solde de l'agent est de : " + resultat.getJoursDeCongesSansSolde() + "\n" +
                                "Le nombre de jours de congés groupés de l'agent est de : " + resultat.getJoursDeCongesGroupes() + "\n" +
                                "Le nombre de jours de congés utilisés de l'agent est de : " + resultat.getJoursDeCongesUtilises());
        }
    }
}
