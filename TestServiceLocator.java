import container.ApplicationContext;
import container.Factory;
import service_locator.*;
import java.util.*;
import java.time.LocalDate;
import Conges_v1.*;
import Conges_v3.*;

public class TestServiceLocator extends junit.framework.TestCase{
    public void testAgentFrance() throws Exception{
        Conges_v1.Mairie m = new Conges_v1.Mairie();
        m.setNom("Lyon");
        m.setEstMetropole(true);
        m.setEstDomTom(false); 
        m.setEstCorse(false);
        
        Conges_v1.Agent a = new Conges_v1.Agent();
        a.setNom("Dupont");
        a.setPrenom("Jacques");
        a.setDateDeNaissance(LocalDate.of(1998, 1, 23));
        a.setMairie(m);
        a.setTypeContrat("TC");
        a.setDebutContrat(LocalDate.of(2011, 2, 21));
        List<String> jours = new ArrayList();
        jours.add("Lundi");
        jours.add("Mardi");
        jours.add("Mercredi");
        jours.add("Jeudi");
        jours.add("Vendredi");
        a.setJoursTravailles(jours);
        a.setPaysOrigine("France");
        a.setDptOrigine("Guyane");
        a.setVilleOrigine("Cayenne");
        a.setPaysOrigineConjoint("France");
        a.setEstMetropole(false);
        a.setEstDomTom(true);
        a.setEstCorse(false);
        a.setJoursCongesUtilises(0);
        a.setPeriodesCongesPris(null);
        a.setJoursCongesCumules(25);
        a.setDemandeCongesBonifies(LocalDate.of(2014, 12, 1));
        
        ApplicationContext ctx = Factory.createApplicationContext("./README.TXT");
        ServiceLocatorI serviceLocator = ctx.getBean("serviceLocator");
        Conges_v1.Resultat resultat = new Conges_v1.Resultat();
        /*
        for(String service : serviceLocator){
            System.out.println("\t" + service);
        }
        */
        
        commandes.Invocateur invocateur = serviceLocator.lookup("invocateur");
        invocateur.executer(a,resultat);
        System.out.println("resultat: " + resultat.getJoursDeConges());
    }
}