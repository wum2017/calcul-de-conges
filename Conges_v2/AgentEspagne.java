package Conges_v2;

public class AgentEspagne extends Agent
{
    private String villeOrigine;
    
    public String getVilleOrigine(){
        return this.villeOrigine;
    }
    
    public void setVilleOrigine(String ville){
        this.villeOrigine = ville;
    }
}