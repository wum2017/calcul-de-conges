package Conges_v2;

import conditions.*;

public class ConditionEstFrance implements ConditionI<Agent>
{
    public boolean estSatisfaite(Agent a){
        return a.getMairie().estFrance();
    }
}