package Conges_v2;

import conditions.*;

public class ConditionEstSeville implements ConditionI<AgentEspagne>
{     
    public boolean estSatisfaite(AgentEspagne a){
        return a.getVilleOrigine().equals("Seville");
    }
}