package Conges_v2;

import container.Factory;
import container.ApplicationContext;
import commandes.Invocateur;
import commandes.CommandeI;
import java.util.*;
import java.time.LocalDate;

public class Test extends junit.framework.TestCase
{
    public void testCongesAgentFrance1() throws Exception{
        ApplicationContext container = Factory.createApplicationContext("./Conges_v2/README.TXT");
        
        Mairie m = new Mairie();
        m.setNom("Lyon");
        m.setPays("France");
        
        AgentFrance a = new AgentFrance();
        a.setNom("Dupont");
        a.setPrenom("Jacques");
        a.setDateDeNaissance(LocalDate.of(1998, 1, 23));
        a.setMairie(m);
        a.setTypeContrat("TC");
        a.setDebutContrat(LocalDate.of(2011, 2, 21));
        List<String> jours = new ArrayList();
        jours.add("Lundi");
        jours.add("Mardi");
        jours.add("Mercredi");
        jours.add("Jeudi");
        jours.add("Vendredi");
        a.setJoursTravailles(jours);
        
        Resultat resultat = new Resultat();
             
        try{
            Invocateur invocateur = container.getBean("invocateur");
            invocateur.executer(a, resultat);
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("Exception ! " + e.getMessage());
        }finally{
            System.out.println("resultat: Le nombre de jours de congés de l'agent est de : "+ resultat.getJoursDeConges());
        }
    }
    
    public void testCongesAgentEspagne1() throws Exception{
        ApplicationContext container = Factory.createApplicationContext("./Conges_v2/README.TXT");
        
        Mairie m = new Mairie();
        m.setNom("Barcelone");
        m.setPays("Espagne");
        
        AgentEspagne a = new AgentEspagne();
        a.setNom("Gaciot");
        a.setPrenom("Lara");
        a.setDateDeNaissance(LocalDate.of(1986, 3, 31));
        a.setMairie(m);
        a.setVilleOrigine("Seville");
        
        Resultat resultat = new Resultat();
             
        try{
            Invocateur invocateur = container.getBean("invocateur");
            invocateur.executer(a, resultat);
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("Exception ! " + e.getMessage());
        }finally{
            System.out.println("resultat: Le nombre de jours de congés de l'agent est de : "+ resultat.getJoursDeConges());
        }
    }
}