package Conges_v2;

import java.util.*;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;

public class AgentFrance extends Agent
{
    private String        typeContrat;
    private LocalDate     debutContrat;
    private List<String>  joursTravailles; // (lundi, mardi, mercredi, jeudi, vendredi, samedi, dimanche), si TC/TP/TNC
    private List<Periode> periodesTravailles; // si TA
    
    public String getTypeContrat(){
        return this.typeContrat;
    }
      
    public LocalDate getDebutContrat(){
        return this.debutContrat;
    }
      
    public List<String> getJoursTravailles(){
        return this.joursTravailles;
    }
      
    public List<Periode> getPeriodesTravailles(){
        return this.periodesTravailles;
    }
    
    public void setTypeContrat(String typeContrat){
        this.typeContrat = typeContrat;
    }
    
    public void setDebutContrat(LocalDate debutContrat){
        this.debutContrat = debutContrat;
    }
      
    public void setJoursTravailles(List<String> joursTravailles){
        this.joursTravailles = joursTravailles;
    }
      
    public void setPeriodesTravailles(List<Periode> periodesTravailles){
        this.periodesTravailles = periodesTravailles;
    }
    
    // Nombre de mois travaillés pour l'année en cours
    public long getMoisTravailles(){
        LocalDate now = LocalDate.now(ZoneId.of("Europe/Paris"));
        LocalDate begin = LocalDate.of(now.getYear(), 1, 1);
        long m;
        if (this.debutContrat.compareTo(begin) < 0){
            m = Period.between(begin, now).toTotalMonths();
        }
        else{
            m = Period.between(this.debutContrat, now).toTotalMonths();
        }
        return m;
    }
}