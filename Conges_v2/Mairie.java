package Conges_v2;

public class Mairie
{
    private String nom;
    private String pays;
    
    public String getNom(){
        return this.nom;
    }
    
    public String getPays(){
        return this.pays;
    }
    
    public void setNom(String nom){
        this.nom = nom;
    }
    
    public void setPays(String pays){
        this.pays = pays;
    }
    
    public boolean estFrance(){
        return this.pays.equals("France");
    }
    
    public boolean estEspagne(){
        return this.pays.equals("Espagne");
    }
}